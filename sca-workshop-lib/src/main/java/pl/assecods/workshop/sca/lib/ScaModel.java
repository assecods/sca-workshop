package pl.assecods.workshop.sca.lib;

import lombok.Data;

/**
 * Created by kacper.sobisz@assecods.pl on 08.05.2023
 */
@Data
public class ScaModel {

    private String id;

}