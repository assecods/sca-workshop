package pl.assecods.workshop.sca.app;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@Slf4j
@SpringBootTest
class ScaApplicationTests {

    @Test
    void contextLoads() {
        log.info("just load context");
    }

}
