package pl.assecods.workshop.sca.app;

import java.util.Map;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Created by kacper.sobisz@assecods.pl on 06.05.2023
 */
@Configuration
public class ScaRouter {

    @Bean
    public RouterFunction<ServerResponse> scaRoutes(ScaLinkHandler handler) {
        return RouterFunctions.route()
                              .GET(RequestPredicates.path("info"), handler::retrieveLinks)
                              .GET(RequestPredicates.path("report"), handler::retrieveReport)
                              .build();
    }

    @Component
    @ConfigurationProperties(prefix = "sca")
    static class ScaLinkHandler {

        @Setter
        private Map<String, String> info;
        @Setter
        private String reportPath;

        private final ResourceLoader resourceLoader;

        public ScaLinkHandler(ResourceLoader resourceLoader) {
            this.resourceLoader = resourceLoader;
        }

        Mono<ServerResponse> retrieveLinks(ServerRequest serverRequest) {
            return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).bodyValue(info);
        }

        Mono<ServerResponse> retrieveReport(ServerRequest serverRequest) {
            String reportFormat = serverRequest.queryParam("format").orElse("html");
            Flux<DataBuffer> report = DataBufferUtils.read(resourceLoader.getResource(String.format(reportPath, reportFormat)),
                                                           new DefaultDataBufferFactory(),
                                                           4096);
            return ServerResponse.ok().body(report, DataBuffer.class);
        }
    }

}
